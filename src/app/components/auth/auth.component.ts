import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  title
} from 'process';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NzNotificationService, NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.less']
})
export class AuthComponent implements OnInit {

  signUpForm: FormGroup;
  loginForm: FormGroup;

  constructor(
    private authServ:AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private notification: NzNotificationService,
    private message: NzMessageService,) {}

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      username: new FormControl("", Validators.required),
      email: new FormControl("",
        [Validators.required, Validators.maxLength(250),
          Validators.minLength(5), Validators.email
        ]),
      password: new FormControl("",
        [Validators.required, Validators.maxLength(250),
          Validators.minLength(8)
        ]),
      checkPassword: new FormControl("", [Validators.required, this.confirmValidator])
    });
    this.loginForm = new FormGroup({
      email: new FormControl("",
        [Validators.required, Validators.maxLength(250),
          Validators.minLength(5), Validators.email
        ]),
      password: new FormControl("",
        [Validators.required, Validators.maxLength(250),
          Validators.minLength(8)
        ]),
    });
  }

  onRegister() {
    const username = this.signUpForm.value.username;
    const email = this.signUpForm.value.email;
    const password = this.signUpForm.value.password;

    this.authServ.signUp({username:username,email:email,password:password})
    .subscribe(
      () => {
        
        this.router.navigate(['tasks']);
      },
      err => {

        if (err.error instanceof ErrorEvent) {
          this.message.create('error',
            `${err.error.message}`
          );
        } else {
          console.log(err);
          if (!window.navigator.onLine) {
            this.notification.create(
              'error',
              `Internet connection lost`,
              ''
            );
            return
          }
          this.message.create('error',
            `${err.error} `);
        }

      }
    );
   
  }

  onLogin() {
    const email = this.loginForm.value.email;
    const password = this.loginForm.value.password;
    
    this.authServ.login({email:email,password:password})
    .subscribe(
      () => {
        this.router.navigate(['tasks']);  
      },
      err => {

        if (err.error instanceof ErrorEvent) {
          this.message.create('error',
            `${err.error.message}`
          );
        } else {
          console.log(err);
          if (!window.navigator.onLine) {
            this.notification.create(
              'error',
              `Internet connection lost`,
              ''
            );
            return
          }
          this.message.create('error',
            `${err.error} `);
        }
      }
    )
  }

  updateConfirmValidator() {
    Promise.resolve().then(() => {
      this.signUpForm.controls.checkPassword.updateValueAndValidity()
    });
  }

  confirmValidator = (control: FormControl): {
    [s: string]: boolean
  } => {
    if (!control.value) {
      return {
        required: true
      };
    }

    //arrow function used to make 'this' in line below refer to the current class 
    else if (control.value !== this.signUpForm.controls.password.value) {
      return {
        confirm: true,
        error: true
      };
    }

    return {}
  }

}
