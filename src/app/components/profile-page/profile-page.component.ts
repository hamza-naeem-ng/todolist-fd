import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.less']
})
export class ProfilePageComponent implements OnInit,OnDestroy {

  username:string;
  email:string;
  assignDataSubs:Subscription;

  constructor(private authServ:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.assignDataSubs = this.authServ.user.subscribe(
      user=>{
        if(user){
          this.username = user.username;
          this.email = user.email;
        }
      }
    );
  }

  return(){
    this.router.navigate(['tasks']);
  }

  ngOnDestroy(){
    this.assignDataSubs.unsubscribe();
  }

}
