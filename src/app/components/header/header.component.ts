import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  user:User;
  constructor(private router:Router,private authServ:AuthService) { }

  ngOnInit(): void {
    this.authServ.user.subscribe(
      user=>{
        this.user =user;
      }
    )
  }

  logout(){
    this.authServ.logout();
    this.router.navigate(['auth']); 

  }

  showProfile(){
    this.router.navigate(['profile']);  
  }

}
