import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import {
  Observable
} from 'rxjs';
import {
  AuthService
} from '../services/auth.service';
import {
  take, map
} from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({providedIn:"root"})
export class AuthGuard implements CanActivate {

  constructor(private authServ: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot
  ): boolean | UrlTree | Promise < boolean | UrlTree > | Observable < boolean | UrlTree > {

    return this.authServ.user.pipe(
      take(1),
      map(user => {
          //if !user (user is null) is true making another ! gives a false to isAuthenticated
          const isAuth = !!user;
          if (isAuth) {
            return true
          }
          return this.router.createUrlTree(['auth']);

        }
      )
    )

  }
}
