import {
  Component,
  OnInit,
} from '@angular/core';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  AuthService
} from './services/auth.service';
import {
  Subscription
} from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title: any;
  showHeaderSubs: Subscription;
  loggedIn: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authServ: AuthService) {}


  ngOnInit() {
    this.authServ.autoLogin();
    this.showHeaderSubs = this.authServ.user.subscribe(
      user => {
        if (user) {
          this.loggedIn = true;
        }
        else{
          this.loggedIn =false;
        }
      }
    );
  }

  handleRoute() {
    this.router.navigate(['tasks'], {
      relativeTo: this.route
    });
  }


}
