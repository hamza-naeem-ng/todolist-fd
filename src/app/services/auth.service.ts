import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { tap } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user = new BehaviorSubject<User>(null);
  baseUrl = environment.baseUrl;
  userId:string = null;

  constructor(
    private http:HttpClient,
    
    ) { }

  signUp(user:{username:string,email:string,password:string}){
    const url = `${this.baseUrl}/users`
    return this.http.post<User>(url,user,{ observe: 'response' })
    .pipe(tap(
      res=>{
        this.handleAuth(res.body._id,res.body.username,res.body.email,res.headers.get('x-auth-token'))
       
      }
    ));
  }

  login(user:{email:string,password:string}){
    const url = `${this.baseUrl}/auth`
    return this.http.post<User>(url,user,{observe:'response'})
    .pipe(tap(
      res=>{
        this.handleAuth(res.body._id,res.body.username,res.body.email,res.headers.get('x-auth-token'))
        
      }
    ));
  }

  autoLogin(){
    const userData:{_id:string,username:string,email:string,_token:string}
     = JSON.parse(localStorage.getItem('userInfo'));
    if(!userData){
      return;
    }
    const loadedUser = new User(userData._id,userData.username,userData.email,userData._token);
    this.user.next(loadedUser);
    this.userId = loadedUser._id;
  }

  logout(){
    this.user.next(null);
    this.userId = null;
    localStorage.removeItem('userInfo');
  }

  handleAuth(_id:string,username:string,email:string,token:string){
    let user = new User(_id,username,email,token);
    this.user.next(user);
    this.userId = user._id;
    localStorage.setItem('userInfo',JSON.stringify(user));
  }

  getUserId(){
    return this.userId;
  }
}
