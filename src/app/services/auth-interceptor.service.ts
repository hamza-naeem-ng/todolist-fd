import {
  Injectable
} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpHeaders
} from '@angular/common/http';
import {
  AuthService
} from './auth.service';
import {
  exhaustMap,
  take
} from 'rxjs/operators';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authServ: AuthService) {}
  intercept(req: HttpRequest < any > , next: HttpHandler) {
    // we had to return the original httpReq called inside user.subscribe--if not piped
    // but we could not return from inside a subscribe so we used take operator through piping it,
    // and it will subscribe/catch emitted values of user only 'once' and then destroyed
    return this.authServ.user.pipe(
      take(1),
      exhaustMap(
        user => {
          if (!user) {
            return next.handle(req);
          }
            const reqModified = req.clone({
            headers: new HttpHeaders().set('x-auth-token', user.token)
          });
          return next.handle(reqModified);
        }
      )
    )
  }
}
