import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/models/task.model';
import { DataHandleService } from '../../services/data-handle.service';
import { NzNotificationService, NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.less']
})
export class TaskListComponent implements OnInit {
  @Input() taskList:Task[][]=[];
  
  constructor(private dhs:DataHandleService, 
    private notification: NzNotificationService,
    private message: NzMessageService,) { }

  ngOnInit(): void {
    console.log("created");
    
    setTimeout(()=>{console.log(this.taskList);},2000)
  }

  showDate(date:string){
    return new Date(date).toDateString();
  }

  refresh(){
    this.dhs.fetchTaskList(new Date(new Date().toDateString()).toISOString()).subscribe(
      taskList=>{
        this.taskList = taskList;
        console.log(taskList);
      },
      err => {

      if (err.error instanceof ErrorEvent) {
        this.message.create('error',
          `${err.error.message}`
        );
      } else {
        console.log(err);
        if (!window.navigator.onLine) {
          this.notification.create(
            'error',
            `Internet connection lost, Tasks Could Not be Fetched`,
            ''
          );
          return
        }
        this.message.create('error',
          `${err.error} 
          Tasks could not be fetch due to some Server Error`);
      }

    }
    );
  }

}
