import {
  DataHandleService
} from '../../services/data-handle.service';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import {
  Task
} from '../../../models/task.model';
import {
  NzMessageService, NzNotificationService
} from 'ng-zorro-antd';
import { Subscription } from 'rxjs';
import { TaskManagementService } from '../../services/task-management.service';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.less']
})
export class TaskComponent implements OnInit, OnDestroy {

  @Input() task: Task;
  @Output() taskEditor = new EventEmitter < Task > ();
  @Output() taskRemover = new EventEmitter < string > ();
  h: number = 0;
  m: number = 0;
  s: number = 0;
  timeSpent: string;
  interval;
  timeKeeper: boolean = false;
  hideControls: boolean = false;
  hideControlSubs: Subscription;

  constructor(private dhs: DataHandleService, 
    private message: NzMessageService,
    private notification: NzNotificationService,
    private tms: TaskManagementService) {}

  ngOnInit(): void {
    this.hideControlSubs = this.tms.hideControls.subscribe(
      hide=>{
        this.hideControls = hide;
      }
    );
    this.extractTime();

  }

  editTask() {
    this.taskEditor.emit(this.task);
  }

  startTimer() {
    //timeKeeper is used to avoid multiple instances of setInerval function 
    //This will ensure that unless the user will not call stop timer a instance
    // of setInterval is not called
    if (this.timeKeeper == false) {
      this.timeKeeper = true;
      this.interval = setInterval(() => {
        if (this.s < 59) {
          this.s++
       
         } else if (this.m < 59) {
          this.m++;
          this.s = 0;
         } else if (this.h < 23) {
          this.h++;
        }
        //This complex string interpolation is to only
        //add a 0 before each time section(h,m,s) when they are less than 10
        let hours =`${this.h<10?`0${this.h}`:`${this.h}`}h`
        let minutes =`${this.m<10?`0${this.m}`:`${this.m}`}m`
        let seconds = `${this.s<10?`0${this.s}`:`${this.s}`}s`
        this.timeSpent = `${hours} ${minutes} ${seconds}`;
      

      }, 1000);
    } 
  
  }

  stopTimer() {
    this.timeKeeper = false;
    clearInterval(this.interval);
 }

  saveTime() {

    this.task.timeSpent = this.timeSpent;
    
    this.dhs.updateTask(this.task).subscribe(
      () => {
        this.message.create('success', `Task Updated`);
      },
      err => {
        if (err.error instanceof ErrorEvent) {
          this.message.create('error',
            `${err.error.message}`
          );
        } else {
          console.log(err);
          if(!window.navigator.onLine){
            this.notification.create(
              'error',
             `Internet connection lost`,
              ''
            );
            return  
          }
          this.message.create('error',
            `${err.error}, task was not successfully updated`);
        }
      }
    )
  }

  deleteTask() {
    this.taskRemover.emit(this.task._id);
  }

  extractTime(): void {
    
    let time = this.task.timeSpent.match(/\d+/g).map(Number);
    this.h = time[0];
    this.m = time[1];
    this.s = time[2];

    let hours =`${this.h<10?`0${this.h}`:`${this.h}`}h`
    let minutes =`${this.m<10?`0${this.m}`:`${this.m}`}m`
    let seconds = `${this.s<10?`0${this.s}`:`${this.s}`}s`
    this.timeSpent = `${hours} ${minutes} ${seconds}`;
  }

  ngOnDestroy(){
    //we called stop timer here for a specific scenario i.e when a user starts 
    //timer and then shifts to another date without stopping the timer.
    this.stopTimer();
    this.hideControlSubs.unsubscribe();
  }
}
