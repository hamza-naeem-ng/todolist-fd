import {
  RouterTestingModule
} from '@angular/router/testing';
import {
  Task
} from '../../../models/task.model';
import {
  DataHandleService
} from '../../services/data-handle.service';
import {
  TaskManagementService
} from '../../services/task-management.service';
import {
  Component,
  OnInit,
  Input,
  OnDestroy
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  Subscription
} from 'rxjs';
import {
  NzMessageService, NzNotificationService
} from 'ng-zorro-antd';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'add-task',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.less']
})
export class AddModalComponent implements OnInit, OnDestroy {

  isVisible: boolean;
  editMode: boolean = false;
  isOkLoading: boolean = false;
  disableTimeSpent: boolean;
  task: Task;
  taskForm: FormGroup;
  @Input() date: string;
  
  modalType: string;
  visibleSubs: Subscription;
  editModeSubs: Subscription;
  disableControlSubs: Subscription;

  userId: string = null;

  constructor(private tms: TaskManagementService,
     private dhs: DataHandleService,
      private message: NzMessageService,
      private notification: NzNotificationService,
      private authServ: AuthService) {}

  dateShow(){
    return new Date(this.date).toDateString();
  }

  ngOnInit(): void {
    //visibleSubs provides us 2 things:
    //whether or not to show the modal component using isVisisble
    //and the title of modal(Add Task or Edit) using modal type
    this.visibleSubs = this.tms.modalViewer.subscribe(
      modalViewInfo => {
        this.isVisible = modalViewInfo.visible;
        this.modalType = modalViewInfo.modalType;
      }
      
    );
    //editModeSubs provides us 2 things:
    //the modal is now in edit mode
    //the task to be edited
    this.editModeSubs = this.tms.editMode.subscribe(
      values => {
        this.editMode = values.mode;
        this.task = values.task;
        this.initForm();
      }
    );
    //below line coded bcz editModeSubs will be called only if edit button is clicked
      if(!this.editMode) this.initForm();

      this.disableControlSubs = this.tms.disableControl.subscribe(
        disabled =>{
          this.disableTimeSpent = disabled;
        }
      );

    // to get userId
    this.userId = this.authServ.getUserId();
     
}

  initForm() {

    let title = "";
    let description = "";
    let timeSpent = new Date(0,0,0);

    if (this.editMode) {
      title = this.task.title;
      if (this.task.description) description = this.task.description;
      let time = this.task.timeSpent.match(/\d+/g).map(Number);
      timeSpent = new Date(0, 0, 0, time[0], time[1], time[2]);
    }

    this.taskForm = new FormGroup({
      title: new FormControl(title, [Validators.minLength(3), Validators.maxLength(40), Validators.required]),
      description: new FormControl(description, [Validators.maxLength(250)]),
      timeSpent: new FormControl(timeSpent, Validators.required)
    });
    
  }


  onSubmit(): void {

    this.isOkLoading = true;
    setTimeout(() => {
      this.isOkLoading = false;
    }, 2000);

    let timeSelected = this.timeStringify();
    
    // making a newTask object to be passed in the http request
    let newTask = {
      date: this.date,
      title: this.taskForm.value['title'],
      timeSpent: timeSelected
    }

    let description = this.taskForm.value['description'];
    if (description.length > 0) {
      newTask['description'] = description;
    }

    if(this.userId){
      newTask['userId'] = this.userId;
    }
    
    if (this.editMode) {
      //id is only neede when we want to edit a task
      newTask['_id'] = this.task._id;
      this.dhs.updateTask(newTask).subscribe(
        (task) => {
          this.tms.fetchSignal.next();
          this.message.create('success', `Task Edited`);
          this.handleCancel();
         },
        (err) => {
          this.notifyError(err,"edited");
        }
      );
    } else {
      this.dhs.postTask(newTask).subscribe(
        (task) => {
          this.reset();
          this.tms.fetchSignal.next();
          this.message.create('success', `Task Added`);
        },
        (err) => {
         this.notifyError(err,"added");
        }
      );
    }
}

  timeStringify() {
    let h = this.taskForm.value['timeSpent'].getHours();
    let m = this.taskForm.value['timeSpent'].getMinutes();
    let s = this.taskForm.value['timeSpent'].getSeconds();
    
    //This complex string interpolation is to only
    //add a 0 before each time section(h,m,s) when they are less than 10
    let hours =`${h<10?`0${h}`:`${h}`}h`
    let minutes =`${m<10?`0${m}`:`${m}`}m`
    let seconds = `${s<10?`0${s}`:`${s}`}s`
    return `${hours} ${minutes} ${seconds}`;
    
  }


  handleCancel(): void {
    this.tms.modalViewer.next({visible:false,modalType:this.modalType});
    this.reset();
  }

  reset() {
    this.taskForm.reset({
      title: '',
      description: '',
      timeSpent: new Date(2020, 0, 1)
    });
    this.editMode = false;
  }

  notifyError(err,action:string){
    if (err.error instanceof ErrorEvent) {
      this.message.create('error',
        `${err.error.message}`
      );
    } else {
      console.log(err);
      if(!window.navigator.onLine){
        this.notification.create(
          'error',
         `Internet connection lost`,
          ''
        );
        return  
      }
      this.message.create('error',
        `${err.error}, task was not successfully ${action}`);
    }
  }

  ngOnDestroy() {
    this.visibleSubs.unsubscribe();
    this.editModeSubs.unsubscribe();
    this.disableControlSubs.unsubscribe();

  }

}
