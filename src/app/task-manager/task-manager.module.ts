import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskManagerComponent } from './task-manager.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FullCalendarModule } from '@fullcalendar/angular';
import { TaskComponent } from './components/task/task.component';
import { AddModalComponent } from './components/add-modal/add-modal.component';
import { RouterModule } from '@angular/router';
import { TaskListComponent } from './components/task-list/task-list.component';


@NgModule({
  declarations: [
    TaskManagerComponent,
    TaskComponent,
    AddModalComponent,
    TaskListComponent],
    
  imports: [
    CommonModule,
    FlexLayoutModule, 
    NgZorroAntdModule,
    ReactiveFormsModule,
    FullCalendarModule, 
    RouterModule.forChild([{path:"", component: TaskManagerComponent}]),
  ],
})
export class TaskManagerModule { }
