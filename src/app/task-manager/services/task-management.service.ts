import { Task } from '../../models/task.model';
import * as Rx from 'rxjs';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TaskManagementService {
  // related to add modal
  modalViewer = new Rx.BehaviorSubject({visible:false,modalType:"Add Task"});
  fetchSignal = new Rx.Subject();
  editMode = new Rx.Subject<{mode:boolean,task:Task}>();

  // related to task
  hideControls = new Rx.BehaviorSubject<boolean>(false);
  disableControl = new Rx.BehaviorSubject<boolean>(false);
  constructor() { }
}
