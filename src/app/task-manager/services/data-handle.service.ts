import { environment } from '../../../environments/environment';
import {  HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Task } from '../../models/task.model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataHandleService {

  baseUrl = environment.baseUrl;
  userId:string = null;

  constructor(private http:HttpClient, private authServ: AuthService) { 
    this.userId = this.authServ.getUserId();
  }

  fetchTasks(date:string): Observable<Task[]> {
    let url = `${this.baseUrl}/tasks/${date}`; 
    const params = new HttpParams().set('userId',`${this.userId}`);
    this.userId = this.authServ.getUserId();
    
    return this.http
      .get<Task[]>(url,{params}).pipe(
        retry(1)
      );
  } 

  fetchTaskList(date:string):Observable<Task[][]>{
    let url = `${this.baseUrl}/tasks/${date}`; 
    const params = new HttpParams().set('list','true').set('userId',`${this.userId}`);
    
    this.userId = this.authServ.getUserId();

    return this.http
      .get<Task[][]>(url,{params}).pipe(
        retry(1)
      );
  }

  deleteTask(id:string){
    let url = `${this.baseUrl}/tasks/${id}`; 
    return this.http.delete<Task>(url);
  }

  postTask(task:Task){
    let url = `${this.baseUrl}/tasks`;
    return this.http.post(url,task);
  }

  updateTask(task:Task){
    let url = `${this.baseUrl}/tasks/${task._id}`;
    return this.http.put(url,task);
    
  }

  
}
