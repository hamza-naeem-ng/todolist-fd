import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { Task } from '../models/task.model';
import { Subscription, BehaviorSubject } from 'rxjs';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { DataHandleService } from './services/data-handle.service';
import { TaskManagementService} from './services/task-management.service';
import { NzNotificationService, NzMessageService } from 'ng-zorro-antd';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.less']
})
export class TaskManagerComponent implements OnInit, OnDestroy,AfterViewInit {

  isClicked:boolean = false;
  isVisible:boolean = false;
  isLoaded: boolean = false;
  hideButton: boolean = false;
  date: string;
  task: Task | null = null;
  tasks: Task[] = [];
  taskList: Task[][]=[];
  current: number = 1;
  fetchSignalSubs: Subscription;
  calendarPlugins = [dayGridPlugin, interactionPlugin];
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;
  
  

  constructor(private dhs: DataHandleService,
    private tms: TaskManagementService,
    private notification: NzNotificationService,
    private message: NzMessageService,
    private authServ: AuthService,
    private router: Router
  ) {}

  ngAfterViewInit(): void {
    let calendar = this.calendarComponent.getApi();
    calendar.setOption('height', 'parent');

  }

  ngOnInit() {
    console.log("created");
    //fetchSignal is used to update the view on when a task is deleted,
    //edited or a new task is added
    this.fetchSignalSubs = this.tms.fetchSignal.subscribe(
      () => {
        this.getTasks(this.date)
      }
    );
    
      this.dhs.fetchTaskList(new Date(new Date().toDateString()).toISOString()).subscribe(
        taskList=>{
          console.log("fetched");
          this.taskList = taskList;
          
        },
        err => {

        if (err.error instanceof ErrorEvent) {
          this.message.create('error',
            `${err.error.message}`
          );
        } else {
          console.log(err);
          if (!window.navigator.onLine) {
            this.notification.create(
              'error',
              `Internet connection lost, Tasks Could Not be Fetched`,
              ''
            );
            return
          }
          this.message.create('error',
            `${err.error} 
            Tasks could not be fetch due to some Server Error`);
        }

      }
      );
      

  }

  handleDateClick(dateClickInfo) {
    
    this.isLoaded = false;
    this.isClicked = true;
    this.date = dateClickInfo.date.toISOString();
    this.getTasks((this.date));
    const currentDate= new Date(new Date().toDateString()).toISOString();
    const clickedDate =new Date(new Date(this.date).toDateString()).toISOString();  
    
    
    if(clickedDate>currentDate){
      this.hideButton = false;
      this.tms.disableControl.next(true);
      this.tms.hideControls.next(true);
    }
    else if(clickedDate<currentDate){
      this.hideButton = true;
      this.tms.disableControl.next(false);
      this.tms.hideControls.next(true);
    }
    else if(clickedDate==currentDate){
      this.hideButton = false;
      this.tms.hideControls.next(false);
      this.tms.disableControl.next(false);
    }

  }

  createTask(): void {
    this.tms.modalViewer.next({
      visible: true,
      modalType: "Add Task"
    });
  }

  editTask(task: Task): void {
    this.tms.editMode.next({
      mode: true,
      task: task
    });

    this.tms.modalViewer.next({
      visible: true,
      modalType: "Edit Task"
    });
  }

  deleteTask(id: string) {
    this.dhs.deleteTask(id).subscribe(
      data => {
        this.message.create('success', `Task Deleted`);
        this.getTasks(this.date);
        console.log(data);
      },
      err => {

        if (err.error instanceof ErrorEvent) {
          this.message.create('error',
            `${err.error.message}`
          );
        } else {
          console.log(err);
          if (!window.navigator.onLine) {
            this.notification.create(
              'error',
              `Internet connection lost`,
              ''
            );
            return
          }
          this.message.create('error',
            `${err.error}, task was not deleted`);
        }

      }

    );

  }

  getTasks(date: string) {
    this.dhs.fetchTasks(date).subscribe(
      tasks => {
        this.tasks = tasks;
        this.isLoaded = true;
        console.log(tasks);
      },
      err => {
        this.isLoaded = true;
        if (err.error instanceof ErrorEvent) {
          this.notification.create(
            'error',
            `${err.error.message}`,
            ''
          );
        } else {
          console.log(err);
          if (!window.navigator.onLine) {
            this.notification.create(
              'error',
              `Internet connection lost`,
              ''
            );
            return
          }
          this.notification.create(
            'error',
            `${err.error}`,
            ''
          );
        }
      }
    );

  }

  ngOnDestroy() {
    this.fetchSignalSubs.unsubscribe();
    console.log("destroyed");
  }

}
