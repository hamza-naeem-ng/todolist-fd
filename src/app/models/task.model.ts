export class Task{
    constructor(public date:string,public title:string,public timeSpent:string, public description? :string, public userId?:string, public _id?:string ){};
}
