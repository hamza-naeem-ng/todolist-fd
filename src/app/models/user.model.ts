export class User {
  constructor(public _id: string,public username:string,public email:string,private _token: string) {}

  get token() {
     if(this._token) return this._token;
  }
}
